import os
import subprocess
import sys
import gi
import threading
import json
import pexpect

# UI library
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
# Mobile GTK widgets
gi.require_version('Handy','1')
from gi.repository import Handy
# Music player control
gi.require_version('Playerctl', '2.0')
from gi.repository import Playerctl, GLib

manager = Playerctl.PlayerManager()
current_player = None
cmd_buffer = []
current_music_info = [None, None, None]

# return true to prevent other signal handlers from deleting objects from the builder
class Handler:
	def _btnQuit(self, *args):
		# exit gtk
		Gtk.main_quit()
		# exit all threads
		os._exit(1)
		return True

	def _btnAbout(self, *args):
		o("windowAbout").show()
		return True

	def _closeAbout(self, *args):
		o("windowAbout").hide()
		return True

# Fuction for grabbing UI objects
def o(name):
	for i in range(0,len(objects)):
		if objects[i].get_name() == name:
			return objects[i]
	return -1

# Start the app
def init():
	print("initializing")
	Gtk.init()
	Handy.init()
	global builder
	builder = Gtk.Builder()
	builder.add_from_file("/app/bin/app.ui")
	builder.connect_signals(Handler())
	global objects
	objects = builder.get_objects()
	o("window").show_all()
	print("initialized")

# Set the time
def rtc():
	o("lblInitializing").set_label("Checking if time is synced...")
	output=subprocess.check_output(['/app/bin/wasptool','--check-rtc'],universal_newlines=True)
	if output.find("delta 0") >= 0:
		print("time is already synced")
	else:
		o("lblInitializing").set_label("Syncing time...")
		#output=subprocess.check_output(['/app/bin/wasptool','--rtc'],universal_newlines=True)
		print(output)
	o("lblInitializing").set_label("Done!")

# Interact with the NUS REPL
def console():
	global current_player
	c = pexpect.spawn('./wasptool --console', encoding='UTF-8')
	c.expect('Connect.*\(([0-9A-F:]*)\)')
	c.expect('Exit console using Ctrl-X')
	print("connected to console")
	while True:
		cmd_exec(c)
		try:
			c.expect('{"t":"music", "n":"\w+"}', timeout=0.1)
			music_commands = {"play": current_player.play, "pause": current_player.pause, "next": current_player.next, "previous": current_player.previous}
			musiccmd = json.loads(c.match.group())
			if current_player:
				action = music_commands[musiccmd["n"]]
				action()
		except:
			pass

# Send commands from the cmd buffer
def cmd_exec(c):
	global cmd_buffer
	global current_player
	global current_music_info
	if cmd_buffer != []:
		for cmd in cmd_buffer:
			print(cmd)
			c.sendline(cmd)
			c.expect(">>>")
		cmd_buffer = []

# Initialise new player
def on_player_appeared(manager, name):
	global current_player
	current_player = Playerctl.Player.new_from_name(name)
	current_player.connect('playback-status::playing', on_play, manager)
	current_player.connect('playback-status::paused', on_pause, manager)
	current_player.connect('metadata', on_metadata_change, manager)
	manager.manage_player(current_player)

# Remove player after it has vanished
def on_player_vanished(manager, player):
	global current_player
	if player == current_player:
		playercount = len(manager.props.player_names)
		if playercount > 0:
			on_player_appeared(manager, manager.props.player_names[playercount-1])
		else:
			current_player = None

# Handle playback status from PC
def on_play(player, status, manager):
	print("PC invoked play")
	cmd_buffer.append('GB({"t":"musicstate","state":"play"})')

def on_pause(player, status, manager):
	print("PC invoked pause")
	cmd_buffer.append('GB({"t":"musicstate","state":"pause"})')

def on_metadata_change(player, metadata, manager):
	print("Metadata changed")
	artist = current_player.get_artist()
	track = current_player.get_title()
	cmd_buffer.append('GB({"t":"musicinfo","artist":"' + artist + '","track":"' + track + '"})')

# UI thread
def threadGtk():
	print("gtk thread started")
	Gtk.main()
	print("gtk thread ended")

# Thread for calling wasptool
def threadWasptool():
	rtc()
	print("rtc is done")
	console()

# Thread for running playerctl
def threadPlayerCtl():
	manager.connect('name-appeared', on_player_appeared)
	manager.connect('player-vanished', on_player_vanished)
	for name in manager.props.player_names:
		on_player_appeared(manager, name)

	main = GLib.MainLoop()
	main.run()

init()
threadG = threading.Thread(target=threadGtk)
threadW = threading.Thread(target=threadWasptool)
threadP = threading.Thread(target=threadPlayerCtl)
threadG.start()
threadW.start()
threadP.start()
